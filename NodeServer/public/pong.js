// Pong game logic



function sprite(initial_rect, canvas) {
    var _rect = initial_rect; // { top, left, bottom, right }
    var _canvas = canvas;
    var _ctx = canvas.getContext("2d");

    function _touches(sprite) {
        var rect = sprite.getrect();
        if (_rect.left < rect.right && _rect.right > rect.left &&
            _rect.top < rect.bottom && _rect.bottom > rect.top) {
            return sprite;
        }
        return false;
    }

    function _toucheslist(sprites) {
        sprites = sprites || [];
        var touched = false;

        sprites.forEach(function(sprite) {

            if (_rect != sprite.getrect() && !touched) {
                touched = _touches(sprite);
            }
        });

        return touched;
    }

    function _draw() {
        _ctx.rect
        _ctx.rect(_rect.left, _rect.top, _rect.right - _rect.left, _rect.bottom - _rect.top);
        _ctx.stroke();
    }

    // Returns true if another object ist touched, false otherwise
    function _moveX(amount, othersprites) {
        /*// prevent shifting out of canvas
        var newamount = Math.min(_canvas.width, _rect.right + amount) - _rect.right;
        newamount = Math.max(0, _rect.left + newamount) - _rect.left;
        */

        var oldright = _rect.right;
        var oldleft = _rect.left;

        _rect.right += amount;
        _rect.left += amount;

        var touched = _toucheslist(othersprites);
        if (touched) {
            _rect.right = oldright;
            _rect.left = oldleft;
        }

        return touched;
    }

    function _moveY(amount, othersprites) {
        /*// prevent shifting out of canvas
        var newamount = Math.min (_canvas.height, _rect.bottom + amount) - _rect.bottom;
        newamount = Math.max (0, _rect.top + newamount) - _rect.top;
        */

        var oldtop = _rect.top;
        var oldbottom = _rect.bottom;

        _rect.top += amount;
        _rect.bottom += amount;

        var touched = _toucheslist(othersprites);
        if (touched) {
            _rect.top = oldtop;
            _rect.bottom = oldbottom;
        }

        return touched;
    }

    function _move(x,y, othersprites) {
        var touches = false;
        touches = touches || _moveX(x, othersprites);
        touches = touches || _moveY(y, othersprites);
        return touches;
    }

    function _setpos(x,y) {
        _rect.right = _rect.right - _rect.left + x;
        _rect.bottom = _rect.bottom - _rect.top + y;
        _rect.top = y;
        _rect.left = x;
    }

    function _setrect(rect) {
        _rect.top = rect.top;
        _rect.bottom = rect.bottom;
        _rect.left = rect.left;
        _rect.right = rect.right;
    }

    return {
        touches : _touches,
        draw : _draw,
        move : _move,
        getrect:   function() { return _rect; },
        setrect: _setrect,
        setpos: _setpos
    }
}

function cPong(canvas) {
    var _canvas = canvas;
    var _ctx = canvas.getContext("2d");
    var _sprites = [];
    var _ball = new sprite({top: _canvas.height/2-5, left: _canvas.width/2-5, bottom: _canvas.height/2+5, right: _canvas.width/2+5}, _canvas);

    var _mypaddel = {
        pos : 'top',
        sprite : new sprite({top: 10, left: 4, bottom:20, right: 50}, _canvas)
    };

    var _paddels = [ _mypaddel ];

    var _bounds = [
        new sprite({top: 0, left: 0, bottom: _canvas.height, right: 0}, _canvas), // left
        new sprite({top: 0, left: _canvas.width, bottom: _canvas.height, right: _canvas.width}, _canvas), // right
        new sprite({top: 0, left: 0, bottom: 0, right: _canvas.width}, _canvas), // top
        new sprite({top: _canvas.height, left: 0, bottom: _canvas.height, right: _canvas.width}, _canvas) // bottom
    ];

    var _ballspeed = { x: 5, y: 5};

    function _getData() {
        var data = {
            paddels : [],
            ball : {
                top : _ball.top,
                left: _ball.left,
                speed: _ballspeed
            }
        }
        _paddels.forEach(function(p) {
            data.paddels.push({
                pos : p.pos,
                rect : p.sprite.getrect()
            });
        })
    }

    function setSprites() {
        sprites=[];
        _bounds.forEach(function(s) { _sprites.push(s)});
        _paddels.forEach(function(s) { _sprites.push(s.sprite)});
        _sprites.push(_ball);
    }

    function _updatePaddel(newpos, newsprite) {
        _paddels.forEach(function(p) {
            if (p.pos == newpos) {
                p.sprite.setrect(p.sprite);
                return;
            }
        });
    }

    function _setData(data) {
        _ball.setpos(data.ball.left, data.ball.top);
        _ballspeed = data.ball.speed;
        data._paddels.forEach(function(p){
           if (p.pos != _mypaddel.pos) {
               _updatePaddel(p.pos, p.rect);
           }
        });
    }

    function _render() {
        _ctx.clearRect(0, 0, _canvas.width, _canvas.height);
        _ctx.beginPath();
        _sprites.forEach(function(element) {
            element.draw()}
        );
    }

    function _moveBall() {
        var touched = _ball.move(_ballspeed.x, _ballspeed.y, _sprites);

        if (touched) {
            _bounds.forEach(function(s, idx) { if (touched == s) {
                if (idx < 2) {
                    // left or right
                    _ballspeed.x *= -1;
                } else {
                    // top or bottom
                    _ballspeed.y *= -1;
                }
            }});

            if (touched == _mypaddel.sprite) {
                if (_mypaddel.pos == 'top' || _mypaddel.pos == 'bottom') {
                    _ballspeed.y *= -1;
                } else {
                    _ballspeed.x *= -1;
                }
            }

        }
        _render();
    }

    function _init() {
        setSprites();

        var commandspeed = 4;

        document.addEventListener('keydown', function(event) {
            event.preventDefault();
            if (_mypaddel.pos === 'top' || _mypaddel.pos === 'bottom') {
                if(event.keyCode == 37) {
                    _mypaddel.sprite.move(-commandspeed, 0, _sprites);
                }
                else if(event.keyCode == 39) {
                    _mypaddel.sprite.move(commandspeed, 0, _sprites);
                }
            } else {
                if(event.keyCode == 40) {
                    _mypaddel.sprite.move(0, commandspeed, _sprites);
                }
                else if(event.keyCode == 38) {
                    _mypaddel.sprite.move(0, -commandspeed, _sprites);
                }
            }

            _render();
        });

        window.setInterval(_moveBall, 100);
    }

    _init();

    return {
        render: _render
    }
}