// Simple web/websocket client
// (c) 2018, Reto Bättig
//
// Implements simple chat application with the simple web socket server
// All the data in the "model" structure is synchronized with all other clients
//

var c = document.getElementById("gamecanvas");
var pong = new cPong(c);

// Logs messages to the DOM and the console
function Log(s) {
    document.getElementById("log").innerHTML += s+"<br>\n";
    console.log(s);
}

// Updates the chat messages, gets called when the model gets updated
function RenderModel() {
    pong.render();
}

// Button handler
function StartHandler(event) {
    event.preventDefault();
    RenderModel();
}

function attachButtonHandler() {
    var form = document.getElementById("inputform");
    form.addEventListener('submit', StartHandler);
}


function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
var myID = uuidv4();

function getClient() {
    var client = null;

    if (typeof model.clients == 'undefined') {
        model.clients=[];
    }

    model.clients.forEach(function(c) {
        if (c.id === myID) {
            return c;
        }
    });

    client = {
        id : myID,
        data: pong.getData()
    };

    model.clients.push(client);

    return client;
}

function setClientData() {
    var client = getClient();
}

function handlemessage(messagedata) {
    if (typeof messagedata.cmd !='undefined') {
        switch (messagedata.cmd) {
            case 'fullupdate' :
                model = messagedata.data;
                break;
            case 'ball' :

            default:
                Log('Got undefined command: '+messagedata.cmd);
        }
    } else {
        Log('got undefined message'+JSON.stringify(messagedata));
    }
}

// Socket connection handler function. Does not need to be changed
// returns only one function:
// mySocketConnection.sendMessage(messagedata)
function InitWebSocketHandler() {

    var ws=false; // the WebSocket connection handler

    function closeWebsockets() {
        if (ws) {
            try {
                ws.close();
            } catch(err) {
                Log("Websocket already closed");
            }
        }
    }

    function initWebsockets() {
        Log("Connecting websocket to "+window.location.host);

        closeWebsockets();

        ws = new WebSocket("ws://"+window.location.host);

        // Handlers
        ws.onopen = function () {
            Log("Websocket open")
            ws.send(JSON.stringify({command: "getdata"}));
        };

        ws.onerror = function (error) {
            Log('WebSocket Error ' + error);
        };

        ws.onclose = function() {
            Log('Websocket Closed');
        };

        // Log messages from the server
        ws.onmessage = function (e) {
            var messagedata=JSON.parse(e.data);
            handlemessage(messagedata);
        };
    }

    // Checks Websockets and reconnects if no connection is available
    function checkWebsockets() {
        try {
            if (ws.readyState == 0 || ws.readyState == 1) {
                // WS is in connecting or open state. Everything ok
                return 0;
            }
        } catch(err) {}

        // WS is not in connectiong or open state
        Log("Connection lost, restarting Websockets")
        RenderModel();
        initWebsockets();
    }

    // Repeat cheching websocket connection every 5 seconds
    window.setInterval(checkWebsockets, 5000);

    // Init the WebSockets for the first time
    initWebsockets();

    // Return the interface
    return {
        sendMessage : function(messagedata) {
            ws.send(JSON.stringify({command: "setdata", model: messagedata}));
        }
    }
};

//initialize page
Log("Logfile:");
attachButtonHandler();
var mySocketConnection = InitWebSocketHandler();

